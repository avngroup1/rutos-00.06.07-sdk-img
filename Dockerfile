# Use Ubuntu 16.04 as the base image
FROM ubuntu:16.04

# Install build-essential and C/C++ build tools
RUN apt-get update && \
    apt-get install -y build-essential \
    git-core libssl-dev libncurses5-dev unzip gawk \
    zlib1g-dev python wget subversion file sudo libtool \
    u-boot-tools vim-common

# Clean up
RUN apt-get clean

# Set user, group and setup sudo
ARG USERNAME=rutx
ARG USER_UID=1000
ARG USER_GID=$USER_UID
RUN groupadd --gid $USER_GID $USERNAME
RUN useradd --uid $USER_GID --gid $USERNAME -m $USERNAME
RUN echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME
RUN chmod 0440 /etc/sudoers.d/$USERNAME

RUN mkdir -p /RUT9XX_R_GPL && \
    chown $USERNAME:$USERNAME /RUT9XX_R_GPL
RUN mkdir data
#Switch to user
USER $USER_UID:$USER_GID

# Copy the tar file into the image
COPY RUT9XX_R_GPL_00.06.07.tar.gz /tmp/

# Change directory to /tmp and untar the file
RUN cd /tmp/ && \
    tar -xvf RUT9XX_R_GPL_00.06.07.tar.gz -C /RUT9XX_R_GPL

#copy config
COPY .config /RUT9XX_R_GPL
#build
RUN cd /RUT9XX_R_GPL && make V=s -j $(nproc)

# remove tar file
USER root
RUN rm /tmp/RUT9XX_R_GPL_00.06.07.tar.gz

